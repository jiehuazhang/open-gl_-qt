#ifndef PICTURESCROLL_H
#define PICTURESCROLL_H

#include <QWidget>      //Widget头文件
#include <QPainter>     //画笔头文件
#include <QDebug>       //打印头文件
#include <QMouseEvent>  //鼠标头文件
#include <QWheelEvent>  //滚轮头文件
#include <QPropertyAnimation>


class PictureScroll : public QWidget
{
    Q_OBJECT

public:
    explicit PictureScroll(QWidget *parent = nullptr);


protected:
    void paintEvent(QPaintEvent *) Q_DECL_OVERRIDE;             //画图程序
    void paintPicture(QPainter &painter, QPixmap Pixmap, int deviation);//画图
    void mousePressEvent(QMouseEvent *Mouse);   //按下
    void mouseMoveEvent(QMouseEvent *Mouse);    //移动
    void mouseReleaseEvent(QMouseEvent *);      //松开
    void wheelEvent(QWheelEvent *event);        //滚轮

private:
    int Now_Value;//当前值
    int Min_Value;//最小值
    int Man_Value;//最大值
    bool isDragging;//鼠标按下标志位
    int Mouse_Press;//鼠标拖动值
    int Deviation;//显示鼠标偏移量
};

#endif // PICTURESCROLL_H
