#ifndef DEMOOPENGLWIDGET_H
#define DEMOOPENGLWIDGET_H

#include <QOpenGLWidget>
#include<QVector>
#include<QOpenGLExtraFunctions>
#include<QOpenGLShaderProgram>
#include<QOpenGLVertexArrayObject>
#include<QOpenGLBuffer>
#include<QMatrix3x3>
#include<QMatrix4x4>
#include<QMouseEvent>
#include<QWheelEvent>

class demoOpenglWidget : public QOpenGLWidget,public QOpenGLExtraFunctions
{
    Q_OBJECT
public:
    explicit demoOpenglWidget(QWidget *parent = nullptr);
    ~demoOpenglWidget();
public:
    double rotate_y=0;
    double rotate_x=0;
    double rotate_z=0;
protected:
    void initializeGL();
    void resizeGL(int width, int height);
    void paintGL();
    QVector<float> loadAscllStl(QString filename,int ratio);//ratio缩放系数
    QVector<float> loadBinStl(std::string filename, int ratio);
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void wheelEvent(QWheelEvent *event);//图片缩放功能

private:
    QVector<float> vertices;
    QVector<float> Position;
    QVector<float> Normal;//读文件时的两个临时变量顶点位置、法向量
    QOpenGLShaderProgram shaderprogram;
    QOpenGLVertexArrayObject VAO;
    QOpenGLBuffer VBO;

    QMatrix4x4 model;
    QMatrix4x4 view;
    QMatrix4x4 projection;

    int verticesCnt;
    GLfloat xtrans,ytrans,ztrans;
    QVector2D mousePos;
    QQuaternion rotation;
};




#endif // DEMOOPENGLWIDGET_H
