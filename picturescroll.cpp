#include "picturescroll.h"

PictureScroll::PictureScroll(QWidget *parent)
    : QWidget(parent),
      Now_Value(1),//当前值
      Min_Value(1),//最小值
      Man_Value(13),//最多值
      isDragging(false),
      Mouse_Press(0),
      Deviation(0)
{
    this->resize(parent->width(),350);//界面尺寸，this的尺寸
}

void PictureScroll::paintEvent(QPaintEvent *)//绘画事件，可以用update跟新
{
    int Width = width() - 1;//获取宽，
    //这里宽度就是第12行的宽度

    //如果偏移了过大，需要滚动到下一个图片
    if((Deviation >= Width/3) && (Now_Value > Min_Value))
    {
        Mouse_Press += Width/3;//偏移初值
        Deviation -= Width/3;
        Now_Value -= 1;
    }
    if((Deviation <= -Width/3) && (Now_Value < Man_Value))
    {
        Mouse_Press -= Width/3;//偏移初值
        Deviation += Width/3;
        Now_Value += 1;
    }

    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing, true);
    paintPicture(painter,QPixmap(QString(":/img/penelope/th-c%1.png").arg(Now_Value)),Deviation);

    if(Now_Value > Min_Value)
        paintPicture(painter,QPixmap(QString(":/img/penelope/th-c%1.png").arg(Now_Value-1)),-Width/3+Deviation);

    if(Now_Value < Man_Value)
        paintPicture(painter,QPixmap(QString(":/img/penelope/th-c%1.png").arg(Now_Value+1)),+Width/3+Deviation);

    //补空位的一个显示
    if(Deviation > 0)//>0是-1，<0是+1
    {
        if(Now_Value > (Min_Value+1))//显示+2号图片
            paintPicture(painter,QPixmap(QString(":/img/penelope/th-c%1.png").arg(Now_Value-2)),-Width/3*2+Deviation);
    }
    else
    {
        if(Now_Value < (Man_Value-1))
            paintPicture(painter,QPixmap(QString(":/img/penelope/th-c%1.png").arg(Now_Value+2)),+Width/3*2+Deviation);
    }
}

void PictureScroll::paintPicture(QPainter &painter, QPixmap Pixmap, int deviation)
{
    int Width = width();//获取宽

    int a = qAbs(deviation/5);//取 （偏移量/比例）的绝对值
    int b = qAbs(deviation/10);
    QPixmap nImg = Pixmap.scaled(Width/3-a,Width/3-a,Qt::KeepAspectRatio,Qt::SmoothTransformation);//缩放功能，4个参数：（长，宽，自己按F1看，自己按F1看）
    QRectF target(Width/3+deviation+b,b,Width/3-a,Width/3-a);//4个参数:(左上顶点X,左上顶多Y,图片长,图片宽)
    QRectF source(0,0,Width/3-a,Width/3-a);//4个参数:(左上顶点X,左上顶多Y,图片长,图片宽)
    painter.drawPixmap(target,nImg,source);//3个参数:(图片位置,图片链接,图片大小)
}

void PictureScroll::mousePressEvent(QMouseEvent *Mouse)
{
    isDragging = true;//记录鼠标按下标志位
    Mouse_Press = Mouse->pos().x();//记录X坐标，因为是上下滑动
}

void PictureScroll::mouseMoveEvent(QMouseEvent *Mouse)
{
    if (isDragging)//鼠标按下，的移动，才算拖动
    {
        Deviation = Mouse->pos().x() - Mouse_Press;//获取便宜量

        int Width = width() - 1;//获取宽

        if (Deviation > (Width) / 3)
        {
            Deviation = (Width) / 3;
        }
        if (Deviation < -(Width) / 3)
        {
            Deviation = -(Width) / 3;
        }
        update();//更新显示
    }
}

void PictureScroll::mouseReleaseEvent(QMouseEvent *)
{
    if (isDragging)
        isDragging = false;

    int Width = width() - 1;//获取宽

    if((Deviation > (Width/3)/2) && (Now_Value > Min_Value))
    {
        Now_Value -= 1;
    }
    if((Deviation < (-Width/3)/2) && (Now_Value < Man_Value))
    {
        Now_Value += 1;
    }
    Deviation = 0;//偏移归零
    update();//更新显示
}

void PictureScroll::wheelEvent(QWheelEvent *event)
{
    if(event->delta() > 0)//向上滚轮
    {
        if(Now_Value > Min_Value)
            Now_Value -= 1;
    }
    else//向下滚轮
    {
        if(Now_Value < Man_Value)
            Now_Value += 1;
    }
    Deviation = 0;
    update();//更新显示
}
