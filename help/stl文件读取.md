# 1.stl文件

## 1.1分类

+ ascll码格式

![image-20211229082909511](Untitled.assets/image-20211229082909511.png)

+ 二进制格式 

![img](https://img-blog.csdnimg.cn/20191122175912286.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MjgzNzAyNA==,size_16,color_FFFFFF,t_70)

举例：![image-20211229085010673](Untitled.assets/image-20211229085010673.png)

![image-20211229083347919](Untitled.assets/image-20211229083347919.png)

![image-20211229083209907](Untitled.assets/image-20211229083209907.png)

显然正方体有六个面，每个面由两个等腰直角三角形拼接而成，故一共有2*6=12个三角形，即下图有12组

![image-20211229083525510](Untitled.assets/image-20211229083525510.png)

下面使用二进制查看器查看机器码：

[在线二进制文件编辑 (markbuild.com)](https://h.markbuild.com/doc/binary-viewer-cn.html)

![image-20211229083902553](Untitled.assets/image-20211229083902553.png)

不过也不一定是32位存储三角形面片信息，如之前下过一个模型，其面片有100060个，即![image-20211229090819764](Untitled.assets/image-20211229090819764.png)

![image-20211229090847215](Untitled.assets/image-20211229090847215.png)

在qt的资源文件里面也可以查看机器码

![image-20211229092442223](Untitled.assets/image-20211229092442223.png)

因为面片溢出了，更改为64字节读取，发现并不可行，并且出现out of memory内存溢出错误

解决办法：配置文件加CONFIG+=resources_big

采取精简面片，此时面片为0xfe0e刚好在32位内，但此时发现文件读取不了，fopen一直打开是空指针。

查了很多博客发现还是无法解决，突然想到之前打开ascll码文件时出现文件内存溢出，再试一下ascll码打开文件，成功。

![image-20211229100630542](Untitled.assets/image-20211229100630542.png)

## 1.2读取ascll码文件

![image-20211229100933185](Untitled.assets/image-20211229100933185.png)

```c++
QVector<float> demoOpenglWidget::loadAscllStl(QString filename, int ratio)
{
    QVector<float> vertices_temp;
    qDebug()<<"load text file:"<<filename;

    QFile file(filename);
    if(!file.open(QIODevice::ReadOnly | QIODevice::Text)){
        qDebug() << "Open stl_file failed." << endl;
    }
    while(!file.atEnd()){
        QString line = file.readLine().trimmed();//trim去除首尾空白字符
        QStringList words = line.split(' ',QString::SkipEmptyParts);

        if(!file.atEnd() && words[0]=="facet"){//看上图知原因
            Normal = {ratio*words[2].toFloat(),ratio*words[3].toFloat(),ratio*words[4].toFloat()};
        }
        else if (!file.atEnd() && words[0] == "vertex") {
            Position = {ratio*words[1].toFloat(), ratio*words[2].toFloat(),ratio*words[3].toFloat()};
            vertices_temp.append(Position);
            vertices_temp.append(Normal);
        }
        else
            continue;
    }
    file.close();

            qDebug() << "write vertice_temp success!" << filename;
            return vertices_temp;
}
```

## 1.3读取二进制文件

![image-20211229102006493](Untitled.assets/image-20211229102006493.png)

```c++
QVector<float> demoOpenglWidget::loadBinStl(std::string filename, int ratio)
{
    QVector<float> vertices_temp;
    qDebug()<<"load text file:"<<filename.c_str();

    FILE *file = fopen(filename.c_str(),"rb");

    //80字节文件头
    char header[80];
    fread(header,80,1,file);
    //4字节三角形面片数量
    uint32_t triangleNum;
    fread(&triangleNum,sizeof(uint32_t),1,file);
    float n1,n2,n3;
    float p1,p2,p3;
    for(auto i=0;i<triangleNum;i++){//读取法向量信息
        fread(&n1,sizeof(float),1,file);
        fread(&n2,sizeof(float),1,file);
        fread(&n3,sizeof(float),1,file);
        qDebug()<<"n1="<<n1<<" n2="<<n2<<" n3="<<n3;
        Normal={n1*ratio,n2*ratio,n3*ratio};

        for(auto j=0;j<3;j++){//读取顶点信息
            fread(&p1,sizeof(float),1,file);
            fread(&p2,sizeof(float),1,file);
            fread(&p3,sizeof(float),1,file);
            qDebug()<<"p1="<<p1<<" p2="<<p2<<" p3="<<p3;
            Position = {p1*ratio,p2*ratio,p3*ratio};
            vertices_temp.append(Position);
            vertices_temp.append(Normal);
        }
        //2字节三角形面片属性
        char c[2];
        fread(c,2,1,file);
    }
    fclose(file);
    delete file;
    qDebug() << "write vertice_temp success!" << filename.c_str();
    return vertices_temp;
}
```

## 1.4常见问题

1. out of memory 内存溢出 在pro配置文件加入CONFIG+=resources_big
2. 三角形面片过多，此时打开blender

选择物体，打开修改器，选择精简

![image-20211229102209564](Untitled.assets/image-20211229102209564.png)

修改塌陷比率，越低三角形面片越少，当然模型越粗糙。

![image-20211229102257463](Untitled.assets/image-20211229102257463.png)

可以看出此时模型三角形清晰可见，面片变少，当然模型非常粗糙。

![image-20211229102354380](Untitled.assets/image-20211229102354380.png)

这样做的目的是：

- ascll码&&二进制文件过大，加载较慢甚至加载不出来，故此步骤相当于**压缩**
- 当面片数过多时，在读取ascll码文件时，在提取完80个字节的头文件信息后，发现三角形面片数量保存在32位4字节中，当面片数量超出0xffff即65535时，面片数量将会以64位存储（甚至更大），但是具体怎么处理我还没有实现。to be continue....

# 1.5总结

模型处理时：

- 坐标中心位于模型中心，方便绕着模型中心旋转
- 面片过大时在blender中修改精简比例
- 输出文件注意是ascll码格式还是二进制文件

工程目录中：

- 内存溢出时加入配置文件：CONFIG+=resources_big
- 目录注意更改（ascll可用相对路径，二进制用绝对（不要出现中文））
- ![image-20211229103034821](Untitled.assets/image-20211229103034821.png)

## 1.3参考博客

[(43条消息) QT OpenGL加载STL模型文件并旋转放缩_GT_L_0813的博客-CSDN博客](https://blog.csdn.net/weixin_46636063/article/details/117526981)

[(43条消息) Qt/C++ + opengl 解析stl文件(二进制和Ascii两种格式)_shenmingyi.blog.csdn.net-CSDN博客_qt stl文件](https://blog.csdn.net/weixin_42837024/article/details/103205080?ops_request_misc=%7B%22request%5Fid%22%3A%22164073488316780264043741%22%2C%22scm%22%3A%2220140713.130102334.pc%5Fall.%22%7D&request_id=164073488316780264043741&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~first_rank_ecpm_v1~rank_v31_ecpm-9-103205080.pc_search_result_cache&utm_term=qt+OpenGL打开stl文件&spm=1018.2226.3001.4187)

## 1.4工程文件

[OpenGL_Qt: 实现加载ascll码&&二进制格式stl文件加载，并在OpenGL控件中展示。 实现了鼠标拖动旋转，滚轮缩放。 注意ascll可用相对路径加载，二进制仅可用绝对路径加载。 值得注意的是二进制文件无法打开过大文件（打开文件指针为空），此项目只适用于较小stl文件读取。 (gitee.com)](https://gitee.com/wuyihengChen/open-gl_-qt)